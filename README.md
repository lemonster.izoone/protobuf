# protobuf

## generate *.pbg.go
- generate proto
```
protoc --go_out=. --go_opt=paths=source_relative -I . pkg/common/request.proto
protoc --go_out=. --go_opt=paths=source_relative -I . pkg/document/document.proto
protoc --go_out=. --go_opt=paths=source_relative -I . pkg/googlebook/book_api.proto
protoc --go_out=. --go_opt=paths=source_relative -I . pkg/springer/book_api.proto
```